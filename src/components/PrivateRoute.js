import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props => (
            sessionStorage.getItem('token')
                ? <Component {...props} />
                : <Redirect to='/' />
        )}
    />
);

PrivateRoute.propTypes = {
    component: PropTypes.node.isRequired
};

export default PrivateRoute;