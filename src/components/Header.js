import React, { Component } from 'react';
import GlobalStyles from '../utils/globalStyles';
import logo from '../assets/logo.svg';
import filterActiveIcon from '../assets/filter-active.svg';
import filterIcon from '../assets/filter.svg';
import { Redirect, Link } from 'react-router-dom';

const styles = {
    filter: {
        width: 'auto',
        height: '4vh',
        marginLeft: '2vw'
    },
    headerMenu: {
        width: '25vw',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginRight: '2vw',
        marginLeft: 'auto'
    },
    headerItem: {
        margin: '2vw auto',
        cursor: 'pointer',
        color: 'white',
        fontSize: '14px',
        fontWeight: 'bold'
    },
    link: {
        textDecoration: 'none',
        margin: '2vw auto',
        cursor: 'pointer',
    },
    linkText: {
        color: 'white',
        fontSize: '14px',
        fontWeight: 'bold'
    }
};

export default class Header extends Component {

    state = {
        isAuth: true
    };

    _onLogOut = () => {
        sessionStorage.clear();
        this.setState({ isAuth: false })

    };

    render() {
        const { isAuth } = this.state;
        if (!isAuth) {
            return (
                <Redirect push to='/' />
            );
        }
        return (
            <header style={GlobalStyles.appHeader}>
                <img src={logo} alt='logobear' style={GlobalStyles.logo} />
                <div style={styles.headerMenu}>
                    <Link to='/jogs' style={styles.link}>
                        <span style={styles.linkText}>Jogs</span>
                    </Link>
                    <Link to='/info' style={styles.link}>
                        <span style={styles.linkText}>Info</span>
                    </Link>
                    <span style={styles.headerItem} onClick={this._onLogOut}>Log out</span>
                    <img src={filterIcon} alt='filter' style={styles.filter} />
                </div>
            </header>
        )
    }
}