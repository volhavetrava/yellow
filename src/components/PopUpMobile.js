import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import GlobalStyles from '../utils/globalStyles';
import greenLogo from '../assets/green-logo.svg';
import crossIcon from '../assets/modal-cross-icon.svg';

class PopUpMobile extends Component {
    render() {
        const {onClose, onLogOut} = this.props;
        return (
            <div style={GlobalStyles.headerMobile.popUp}>
                <div>
                    <div style={GlobalStyles.headerMobile.popUpHeader}>
                        <img src={greenLogo} alt='logobear' style={GlobalStyles.headerMobile.popUpHeaderIcon} />
                        <img
                            src={crossIcon}
                            alt='close'
                            onClick={onClose}
                            style={GlobalStyles.headerMobile.popUpHeaderIcon}
                        />
                    </div>
                    <div style={GlobalStyles.headerMobile.listMenu}>
                        <Link to='jogs' style={GlobalStyles.headerMobile.linkPopUp}>
                            <span style={GlobalStyles.headerMobile.listItemText}>Jogs</span>
                        </Link>
                        <Link to='info' style={GlobalStyles.headerMobile.linkPopUp}>
                            <span style={GlobalStyles.headerMobile.listItemText}>Info</span>
                        </Link>
                        <span style={GlobalStyles.headerMobile.listItemText} onClick={onLogOut}>Log out</span>
                    </div>
                </div>
            </div>
        )
    }
}

PopUpMobile.propTypes = {
    onClose: PropTypes.func.isRequired,
    onLogOut: PropTypes.func.isRequired
};

export default PopUpMobile;