import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Colors from '../utils/colors';
import jogIcon from '../assets/icon.svg';
import { Desktop, Mobile } from '../utils/mediaQueryConsts';

const styles = {
    itemStyle: {
        height: '22vh',
        display: 'flex',
        flexDirection: 'row',
        alignSelf: 'center',
        margin: '8vh 25vw',
        justifyContent: 'center'
    },
    icon: {
        height: '15vh',
        width: 'auto',
        margin: 'auto 5vh'
    },
    jogInfo: {
        display: 'flex',
        flexDirection: 'column'
    },
    greyText: {
        color: Colors.grey,
        fontSize: '14px',
        fontWeight: 500,
    },
    blackText: {
        color: 'black',
        fontSize: '14px',
    },
    spanContainer: {
        display: 'flex',
        flexDirection: 'row'
    },
    itemStyleMobile: {
        height: '22vh',
        display: 'flex',
        flexDirection: 'row',
        alignSelf: 'center',
        margin: '4vh 2vw',
        justifyContent: 'center'
    },
    iconMobile: {
        height: '15vh',
        width: 'auto',
        margin: 'auto 4vh',
        marginLeft: 0

    }
};

const msToDate = (ms) => {
    const date = new Date(ms);
    return date.toDateString();
};

class JogInfo extends Component {
    render() {
        const { item } = this.props;
        return (
            <div>
                <Desktop>
                    <div style={styles.itemStyle}>
                        <img src={jogIcon} alt='icon' style={styles.icon} />
                        <div style={styles.jogInfo}>
                            <span style={styles.greyText}>{`${msToDate(item.date)}`}</span>
                            <span style={styles.spanContainer}>
                        <p style={styles.blackText}>Speed: </p>
                        <p style={styles.greyText}>{`\b ${Math.round(item.distance / (item.time / 60))} km/h`}</p>
                            </span>
                            <span style={styles.spanContainer}>
                        <p style={styles.blackText}>Distance: </p>
                        <p style={styles.greyText}>{`\b ${item.distance} km`}</p>
                            </span>
                            <span style={styles.spanContainer}>
                        <p style={styles.blackText}>Time: </p>
                        <p style={styles.greyText}>{`\b ${item.time} min`}</p>
                            </span>
                        </div>
                    </div>
                </Desktop>
                <Mobile>
                    <div style={styles.itemStyleMobile}>
                        <img src={jogIcon} alt='icon' style={styles.iconMobile} />
                        <div style={styles.jogInfo}>
                            <span style={styles.greyText}>{`${msToDate(item.date)}`}</span>
                            <span style={styles.spanContainer}>
                                <p style={styles.blackText}>Speed: </p>
                                <p style={styles.greyText}>{`\b ${Math.round(item.distance / (item.time / 60))} km/h`}</p>
                            </span>
                            <span style={styles.spanContainer}>
                                <p style={styles.blackText}>Distance: </p>
                                <p style={styles.greyText}>{`\b ${item.distance} km`}</p>
                            </span>
                            <span style={styles.spanContainer}>
                        <p style={styles.blackText}>Time: </p>
                        <p style={styles.greyText}>{`\b ${item.time} min`}</p>
                            </span>
                        </div>
                    </div>
                </Mobile>
            </div>
        );
    }
}

JogInfo.propTypes = {
    item: PropTypes.object.isRequired
};

export default JogInfo;
