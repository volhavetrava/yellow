import React, { Component } from 'react';
import InfiniteList from 'react-infinite-scroll-list';
import PropTypes from 'prop-types';
import JogInfo from './JogInfo';

class JogsList extends Component {
    render() {
        const { jogs } = this.props;
        return (
            <div>
                {jogs.map((item, index) => (
                   <JogInfo item={item} key={index} />
                ))}
            </div>
        );
    }
}

JogsList.propTypes = {
    jogs: PropTypes.array.isRequired
};

export default JogsList;
