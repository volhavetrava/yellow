import {combineReducers} from 'redux';
import login from './login';
import jogs from './jogs';

export default combineReducers({
    login,
    jogs
})