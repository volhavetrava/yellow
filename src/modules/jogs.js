const ACTIONS = {
    ERROR: 'JOGS/ERROR',
    SUCCESS: 'JOGS/SUCCESS',
    LOADING: 'JOGS/SUCCESS'
};

export function jogsError(error) {
    return {
        type: ACTIONS.ERROR,
        error: error,
        isLoading: false
    };
}

export function jogsSuccess(data) {
    return {
        type: ACTIONS.SUCCESS,
        data: data,
        isLoading: false
    };
}

export function jogsLoading() {
    return {
        type: ACTIONS.LOADING,
        isLoading: true
    };
}

const INITIAL_STATE = {
    type: null,
    isLoading: false,
    data: null
};

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case ACTIONS.ERROR:
            return {
                ...state,
                error: action.error
            };
        case ACTIONS.SUCCESS:
            return {
                ...state,
                error: null,
                data: action.data
            };
        case ACTIONS.LOADING:
            return {
                ...state
            };
        default:
            return state;
    }
}