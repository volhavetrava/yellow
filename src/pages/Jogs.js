import React, { Component } from 'react';
import Header from '../components/Header';
import PopUpMobile from '../components/PopUpMobile';
import JogsList from '../components/JogsList';
import sadFace from '../assets/sad-rounded-square-emoticon.svg';
import GlobalStyles from '../utils/globalStyles';
import Colors from '../utils/colors';
import { Mobile, Desktop } from '../utils/mediaQueryConsts';
import logo from '../assets/logo.svg';
import menuIcon from '../assets/menu.svg';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import connect from 'react-redux/es/connect/connect';
import getJogs from '../services/api/getJogsAPI';

const styles = {
    contentSpaceBetween: {
        display: 'flex',
        width: '100vw',
        height: '53vh',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'column',
        padding: '18vh 0'
    },
    greyText: {
        color: Colors.grey,
        lineHeight: '29px',
        fontSize: '24px',
    },
    purpleText: {
        fontSize: '18px',
        color: Colors.purple,
        fontWeight: 'bold',
        fontStyle: 'normal',
        display: 'flex',
        margin: 'auto'
    },
    purpleTextMobile: {
        fontSize: '12px',
        color: Colors.purple,
        fontWeight: 'bold',
        fontStyle: 'normal',
        display: 'flex',
        margin: 'auto'
    },
    buttonPurpleContour: {
        ...GlobalStyles.button,
        backgroundColor: 'transparent',
        borderColor: Colors.purple,
        borderRadius: '36px',
        borderWidth: '3px',
        borderStyle: 'solid',
        width: '27vw',
        height: '7vh',
        display: 'flex',
        margin: '0 auto',
        textAlign: 'center',
    },
    link: {
        ...GlobalStyles.button,
        margin: '2vh auto',
    }
};

class Jogs extends Component {

    state = {
        isPopUpVisible: false,
        isAuth: true
    };

     componentDidMount () {
        const { getJogs } = this.props;
        const token = sessionStorage.getItem('token');
        getJogs(token);
    }

    componentDidUpdate () {
        /*TODO: add spinner for loading*/
        const {data, error} = this.props;
        if (data) {
            console.log(data);
        }
        if (error) {
            alert(error);
        }
    }

    _onLogOut = () => {
        sessionStorage.clear();
        this.setState({ isAuth: false })
    };

    _onClosePopUp = () => {
        this.setState({isPopUpVisible: false})
    };

    render() {
        const { isAuth, isPopUpVisible } = this.state;
        const { data } = this.props;
        if (!isAuth) {
            return (
                <Redirect push to='/' />
            );
        }
        if (isPopUpVisible) return (
            <PopUpMobile onClose={this._onClosePopUp} onLogOut={this._onLogOut} />
        );
        else {
            return (
                <div style={GlobalStyles.mainContainer}>
                    <Desktop>
                        <Header />
                    </Desktop>
                    <Mobile>
                        <header style={GlobalStyles.appHeader}>
                            <img src={logo} alt='logobear' style={GlobalStyles.logo} />
                            <div style={GlobalStyles.headerMobile.headerMenu}>
                                <img
                                    src={menuIcon}
                                    alt='menu'
                                    style={GlobalStyles.headerMobile.menuIcon}
                                    onClick={() => this.setState({ isPopUpVisible: true })}
                                />
                            </div>
                        </header>
                    </Mobile>
                    { !data && (
                        <div style={styles.contentSpaceBetween}>
                            <div>
                                <img src={sadFace} alt='sad face' />
                                <p style={styles.greyText}>Nothing is here</p>
                            </div>
                            <button style={styles.buttonPurpleContour}>
                                <Mobile>
                                    <p style={styles.purpleTextMobile}>Create your jog first</p>
                                </Mobile>
                                <Desktop>
                                    <p style={styles.purpleText}>Create your jog first</p>
                                </Desktop>
                            </button>
                        </div>
                    )
                    }
                    { data && (
                        <JogsList jogs={data.jogs} />
                    )
                    }
                </div>
            );
        }
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getJogs: (token) => dispatch(getJogs(token))
    };
};

const mapStateToProps = state => {
    return {
        isLoading: state.jogs.isLoading,
        data: state.jogs.data,
        error: state.jogs.error
    }
};

Jogs.propTypes = {
    data: PropTypes.object,
    error: PropTypes.object,
    getJogs: PropTypes.func.isRequired,
};

Jogs.defaultProps = {
    error: null,
    data: null
};

export default connect(mapStateToProps, mapDispatchToProps)(Jogs);
