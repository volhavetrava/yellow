import React, { Component } from 'react';
import Header from '../components/Header';
import GlobalStyles from '../utils/globalStyles';
import Colors from '../utils/colors';
import { Mobile, Desktop } from '../utils/mediaQueryConsts';
import logo from '../assets/logo.svg';
import menuIcon from '../assets/menu.svg';
import { Redirect } from 'react-router-dom';
import PopUpMobile from '../components/PopUpMobile';

const styles = {
    content: {
        display: 'flex',
        width: '45vw',
        height: '53vh',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'column',
        padding: '10vh 0',
        alignSelf: 'center',
        margin: '0 auto'
    },
    contentMobile: {
        display: 'flex',
        width: '90vw',
        height: '70vh',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'column',
        padding: '0 0',
        alignSelf: 'center',
        margin: '0 auto'
    },
    hText: {
        color: Colors.appleGreen,
        display: 'flex',
        fontSize: '36px',
        fontWeight: 'bold',
        alignSelf: 'flex-start',
        textTransform: 'uppercase',
    },
    infoText: {
        fontSize: '14px'
    },
};

const infoTextFirst = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ' +
    'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown ' +
    'printer took a galley of type and scrambled it to make a type specimen book. It has survived not' +
    ' only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ' +
    'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,' +
    ' and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';
const infoTextSecond = 'It is a long established fact that a reader will be distracted by the readable content' +
    ' of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal ' +
    'distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.';

class Info extends Component {

    state = {
        isPopUpVisible: false,
        isAuth: true
    };

    _onLogOut = () => {
        sessionStorage.clear();
        this.setState({ isAuth: false })

    };

    _onClosePopUp = () => {
        this.setState({isPopUpVisible: false})
    };

    render() {
        const { isAuth, isPopUpVisible } = this.state;
        if (!isAuth) {
            return (
                <Redirect push to='/' />
            );
        }
        if (isPopUpVisible) return (
            <PopUpMobile onClose={this._onClosePopUp} onLogOut={this._onLogOut} />
        );
        else {
            return (
                <div style={GlobalStyles.mainContainer}>
                    <Desktop>
                        <Header />
                        <div style={styles.content}>
                            <h1 style={styles.hText}>Info</h1>
                            <p style={styles.infoText}>{infoTextFirst}</p>
                            <p style={styles.infoText}>{infoTextSecond}</p>
                        </div>
                    </Desktop>
                    <Mobile>
                        <header style={GlobalStyles.appHeader}>
                            <img src={logo} alt='logobear' style={GlobalStyles.logo} />
                            <div style={GlobalStyles.headerMobile.headerMenu}>
                                <img
                                    src={menuIcon}
                                    alt='menu'
                                    style={GlobalStyles.headerMobile.menuIcon}
                                    onClick={() => this.setState({ isPopUpVisible: true })}
                                />
                            </div>
                        </header>
                        <div style={styles.contentMobile}>
                            <h1 style={styles.hText}>Info</h1>
                            <p style={styles.infoText}>{infoTextFirst}</p>
                            <p style={styles.infoText}>{infoTextSecond}</p>
                        </div>
                    </Mobile>
                </div>
            );
        }
    }
}

export default Info;
