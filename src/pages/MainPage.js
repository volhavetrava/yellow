import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import logo from '../assets/logo.svg';
import bearImage from '../assets/bear-face.svg';
import bearImagePurple from '../assets/bear-face-purple.svg';
import GlobalStyles from '../utils/globalStyles';
import Colors from '../utils/colors';
import { Desktop, Mobile } from '../utils/mediaQueryConsts';
import login from '../services/api/loginAPI';

const styles = {
    purpleForm: {
        backgroundColor: Colors.purple,
        width: '30vw',
        height: '37vh',
        borderRadius: '44px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    bearFace: {
        height: '40%',
        width: 'auto',
    },
    buttonContour: {
        ...GlobalStyles.button,
        backgroundColor: 'transparent',
        borderColor: 'white',
        borderRadius: '36px',
        borderWidth: '3px',
        borderStyle: 'solid',
        width: '30%',
        height: '16%',
        display: 'flex',
        margin: '0 auto',
        textAlign: 'center',
    },
    letMeText: {
        fontSize: '18px',
        color: 'white',
        fontWeight: 'bold',
        fontStyle: 'normal',
        display: 'flex',
        margin: 'auto'
    },
    buttonPurpleContour: {
        ...GlobalStyles.button,
        backgroundColor: 'transparent',
        borderColor: Colors.purple,
        borderRadius: '36px',
        borderWidth: '3px',
        borderStyle: 'solid',
        width: '43vw',
        height: '9vh',
        display: 'flex',
        margin: '0 auto',
        textAlign: 'center',
    },
    letMeTextPurple: {
        fontSize: '18px',
        color: Colors.purple,
        fontWeight: 'bold',
        fontStyle: 'normal',
        display: 'flex',
        margin: 'auto'
    },
    bearFacePurple: {
        height: 'auto',
        width: '43vw'
    },
    spaceBetweenMobile: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'column',
        padding: '21vh 0'
    }
};

class MainPage extends Component {

    state = {
      isAuth: false
    };

    componentDidUpdate () {
        /*TODO: add spinner for loading*/
        const {data, error} = this.props;
        if (data) {
            console.log(data);
            sessionStorage.setItem('token', data.access_token);
            this.setState({isAuth: true});
        }
        if (error) {
            alert(error);
        }
    }

    _onLogin = () => {
        const { login } = this.props;
        login();
    };

    render() {
        const {isAuth} = this.state;
        if (isAuth) {
            return (
                <Redirect push to='/jogs' />
            );
        }
        return (
            <div style={GlobalStyles.mainContainer}>
                <header style={GlobalStyles.appHeader}>
                    <img src={logo} alt='logobear' style={GlobalStyles.logo} />
                </header>
                <Desktop>
                    <div style={GlobalStyles.content}>
                        <div style={styles.purpleForm}>
                            <img src={bearImage} alt='bear' style={styles.bearFace} />
                                <button style={styles.buttonContour} onClick={() => this._onLogin()}>
                                    <p style={styles.letMeText}>Let me in</p>
                                </button>
                        </div>
                    </div>
                </Desktop>
                <Mobile>
                    <div style={GlobalStyles.contentMobile}>
                        <div style={styles.spaceBetweenMobile}>
                            <img src={bearImagePurple} alt='purple bear' style={styles.bearFacePurple} />
                                <button style={styles.buttonPurpleContour} onClick={() => this._onLogin()}>
                                    <p style={styles.letMeTextPurple}>Let me in</p>
                                </button>
                        </div>
                    </div>
                </Mobile>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        login: () => dispatch(login())
    };
};

const mapStateToProps = state => {
    return {
        isLoading: state.login.isLoading,
        data: state.login.data,
        error: state.login.error
    }
};

MainPage.propTypes = {
    data: PropTypes.object,
    error: PropTypes.object,
    login: PropTypes.func.isRequired,
};

MainPage.defaultProps = {
    error: null,
    data: null
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
