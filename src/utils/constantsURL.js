export const API_URL = 'https://jogtracker.herokuapp.com/api/';

export const LOGIN_ENDPOINT = 'v1/auth/uuidLogin';
export const GET_JOGS_ENDPOINT = 'v1/data/sync';