import Responsive from 'react-responsive';
import React from 'react';

export const Desktop = props => <Responsive {...props} minWidth={768} />;
export const Mobile = props => <Responsive {...props} maxWidth={767} />;