import Colors from './colors';

export default {
    mainContainer: {
        width: '100vw',
        height: '100vh'
    },
    appHeader: {
        width: '100vw',
        height: '11vh',
        backgroundColor: Colors.appleGreen,
        alignItems: 'center',
        display: 'flex',
    },
    logo: {
        height: '50%',
        width: 'auto',
        marginLeft: '4%',
    },
    content: {
        display: 'flex',
        width: '100vw',
        height: '89vh',
        alignItems: 'center',
        justifyContent: 'center'
    },
    contentMobile: {
        display: 'flex',
        width: '100vw',
        height: '89vh',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    button: {
        textDecoration: 'none',
        backgroundColor: 'transparent',
        border: 'none',
        outline: 'none',
        cursor: 'pointer'
    },
    headerMobile: {
        filter: {
            width: 'auto',
            height: '4vh',
            marginLeft: '2vw'
        },
        headerMenu: {
            width: '25vw',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
            marginRight: '2vw',
            marginLeft: 'auto'
        },
        menuIcon: {
            width: '7vw',
            height: 'auto',
            marginRight: '2vw'
        },
        link: {
            textDecoration: 'none',
            cursor: 'pointer',
        },
        popUp: {
            display: 'flex',
            width: '100vw',
            height: '100vh',
            backgroundColor: 'white',
            flexDirection: 'column'
        },
        popUpHeader: {
            display: 'flex',
            width: '100vw',
            justifyContent: 'space-between'
        },
        popUpHeaderIcon: {
            height: '5vh',
            width: 'auto',
            margin: '4vw',
        },
        listMenu: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            marginTop: '20vh'
        },
        listItemText: {
            textAlign: 'center',
            fontSize: '5vh',
            fontWeight: 'bold',
            color: 'black',
        },
        linkPopUp: {
            textDecoration: 'none',
            margin: '0 auto',
            marginBottom: '4vh'
        }
    }
}