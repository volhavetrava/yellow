import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import PrivateRoute from './components/PrivateRoute';
import MainPage from './pages/MainPage';
import Jogs from './pages/Jogs';
import Info from './pages/Info';
import './App.css';
import reducers from './modules/index';

const store = createStore(reducers, applyMiddleware(thunk));

class App extends Component {
    render() {
        return (
            <Provider store={store}>
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={MainPage} />
                    <PrivateRoute exact path='/jogs' component={Jogs} />
                    <PrivateRoute exact path='/info' component={Info} />
                </Switch>
            </BrowserRouter>
            </Provider>
        );
    }
}

export default App;
