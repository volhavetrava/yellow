import {API_URL, LOGIN_ENDPOINT} from '../../utils/constantsURL';
import {loginLoading, loginError, loginSuccess} from '../../modules/login';

export default function login () {
    return function (dispatch) {
        dispatch(loginLoading());
        let formData = new FormData();
        formData.append('uuid', 'hello');
        fetch(`${API_URL}${LOGIN_ENDPOINT}`, {
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((response) => {
                if (response.error_message) {
                    dispatch(loginError(response.error_message))
                }
                else {
                    dispatch(loginSuccess(response.response));
                }
            })
            .catch((err) => dispatch(loginError(err)));
    };
}