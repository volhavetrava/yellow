import {API_URL, GET_JOGS_ENDPOINT} from '../../utils/constantsURL';
import {jogsError, jogsLoading, jogsSuccess} from '../../modules/jogs';

export default function login (token) {
    return function (dispatch) {
        dispatch(jogsLoading());
        fetch(`${API_URL}${GET_JOGS_ENDPOINT}`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then((response) => response.json())
            .then((response) => {
                if (response.error_message) {
                    dispatch(jogsError(response.error_message))
                }
                else {
                    dispatch(jogsSuccess(response.response));
                }
            })
            .catch((err) => dispatch(jogsError(err)));
    };
}